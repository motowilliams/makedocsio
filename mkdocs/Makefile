GENERATOR=mkdocs
export DOCS_ROOT_DIR=docs
export BUILD_OUTPUT_DIR:=site
export PUBLISH_DIR:=public
export LOCAL_PORT ?= 8000
export BUILD_CMD = mkdocs build
export SERVE_CMD = mkdocs serve -v --dev-addr=0.0.0.0:$(LOCAL_PORT)

.DEFAULT_GOAL := help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sed -E "s/(.*)?Makefile\://g" | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

ifeq ($(OS),Windows_NT)
SHELL := '/usr/bin/sh.exe'
PWD := $(shell sh.exe -c pwd)
else
SHELL := /bin/bash
PWD := $(shell pwd)
endif

export BASH_CMD := $(SHELL) -c
export REPO_ROOT := $(shell [[ -z "${CI}" ]] && [[ -z "${GITHUB_ACTIONS}" ]] && echo '/app' || echo $(PWD) )
export IS_CI := $(shell [[ -z "${CI}" && -z "${GITHUB_ACTIONS}" ]] && echo "false" || echo "true" )
export VERSION ?= $(shell [[ -d ".git" ]] && git tag --sort=v:refname | grep -E '[0-9]' | tail -1 || echo 0.0.0)

export IMAGE_NAME ?= makedocsio-$(GENERATOR)
export IMAGE_VERSION ?= latest
export IMAGE_TAG = $(IMAGE_NAME):$(IMAGE_VERSION)
export ADDITIONAL_BUILD_ARGS ?=

ifeq ($(IS_CI),true)
	export IMAGE_TAG=
	export DOCKER_COMMAND :=
	export IMAGE_NAME :=
else
	export DOCKER_COMMAND := docker run -it \
		-v $(PWD):$(REPO_ROOT) \
		--name $(IMAGE_NAME) \
		--rm \
		-w $(REPO_ROOT)
endif

docs_docker_build: ## Build the docker image for the static site generator
	docker build --pull $(ADDITIONAL_BUILD_ARGS) -t $(IMAGE_TAG) . -f $(DOCS_ROOT_DIR)/Dockerfile

debug: ## Runs the docker image interactively for debugging purposes
	@$(DOCKER_COMMAND) $(ADDITIONAL_BUILD_ARGS) $(IMAGE_TAG) $(CMD)

docs_build: ## Build the static site
	$(eval CMD := cd $(REPO_ROOT)/$(DOCS_ROOT_DIR) && $(BUILD_CMD))
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

docs_publish: ## Publishes the static site the configured directory
	$(eval CMD := mkdir -p $(REPO_ROOT)/$(PUBLISH_DIR) && cp -R $(REPO_ROOT)/$(DOCS_ROOT_DIR)/$(BUILD_OUTPUT_DIR)/* $(REPO_ROOT)/$(PUBLISH_DIR))
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

docs_serve: ## Serves the static site on localhost
	$(eval CMD := \
	cd $(REPO_ROOT)/$(DOCS_ROOT_DIR) && $(SERVE_CMD))
	$(DOCKER_COMMAND) -p $(LOCAL_PORT):$(LOCAL_PORT) --expose $(LOCAL_PORT) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

print-%: ; @echo $*=$($*)

printenv:
	printenv | sort
