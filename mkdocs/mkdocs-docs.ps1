$GENERATOR = "mkdocs"
$COMMIT_HASH = if ($env:COMMIT_HASH) { $env:COMMIT_HASH } else { "0" * 40 };
$ARCHIVE = "$COMMIT_HASH-$GENERATOR-docs.zip"
$ARCHIVEURL = "https://makedocs.io/$ARCHIVE"
$CHECKSUMURL = "https://makedocs.io/checksum.txt"
$SCRIPT_ROOT = if ([string]::IsNullOrWhiteSpace($PSScriptRoot)) { Get-Location } else { $PSScriptRoot }

Write-Host "Downloading"
Write-Host " - archive $CHECKSUMURL sha file to $SCRIPT_ROOT"
(Invoke-RestMethod -Uri $CHECKSUMURL) -split "`n" | Select-String -Raw -Pattern $ARCHIVE | Out-File -FilePath $SCRIPT_ROOT\checksum.txt

Write-Host " - archive $ARCHIVEURL to $SCRIPT_ROOT"
Invoke-RestMethod -Uri $ARCHIVEURL -OutFile "$SCRIPT_ROOT\$ARCHIVE"

Write-Host -NoNewline "Validating checksum of $SCRIPT_ROOT\$ARCHIVE"
$HASH = (Get-FileHash -Path "$SCRIPT_ROOT\$ARCHIVE" -Algorithm SHA256).Hash
if (!(Select-String -Raw -Path "$SCRIPT_ROOT\checksum.txt" -Pattern $HASH -SimpleMatch)) {
    Write-Error 'Download failed hash check!'
    exit
}
Write-Host -ForegroundColor Green " Passed"

Write-Host "Checking for existing docs directory"
if (Test-Path "$SCRIPT_ROOT\docs") {
    Write-Host -ForegroundColor Yellow "$SCRIPT_ROOT\docs already exists"
    Write-Host -ForegroundColor Yellow "Please manually expand $SCRIPT_ROOT\$ARCHIVE and configure the following directory tree structure"
    Write-Host -ForegroundColor Yellow "Repository Root"
    Write-Host -ForegroundColor Yellow "└── docs              # Configurable documentation system directory"
    Write-Host -ForegroundColor Yellow "    ├── Dockerfile    # Dockerfile used to build/serve your site"
    Write-Host -ForegroundColor Yellow "    ├── Makefile      # Sub-Makefile to provide automation"
    Write-Host -ForegroundColor Yellow "    ├── content       # Configurable documentation content directory"
    Write-Host -ForegroundColor Yellow "    │   └── index.md  # The documentation homepage with sample content"
    Write-Host -ForegroundColor Yellow "    └── mkdocs.yml    # Mkdocs configuration file"
    return
}

Write-Host "Extracting"
Write-Host " - archive $ARCHIVE"
Expand-Archive -Path "$SCRIPT_ROOT\$ARCHIVE" -DestinationPath "$SCRIPT_ROOT"

Write-Host "Cleaning up"
Remove-Item -Force -Recurse "$ARCHIVE"
Remove-Item -Force -Recurse "$SCRIPT_ROOT/checksum.txt"
