terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "cloudflare" {
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.account_id
}

resource "cloudflare_record" "a" {
  type    = "A"
  name    = var.a_record_name
  value   = var.a_record_value
  ttl     = 1 #automatic
  zone_id = var.cloudflare_zone_id
  proxied = true
}

resource "cloudflare_record" "cname" {
  type    = "CNAME"
  name    = var.cname_record_name
  value   = var.cname_record_value
  ttl     = 1 #automatic
  zone_id = var.cloudflare_zone_id
  proxied = true
}

resource "cloudflare_worker_route" "conneg_route" {
  zone_id     = var.cloudflare_zone_id
  pattern     = format("%s/*", var.cname_record_value)
  script_name = cloudflare_worker_script.conneg_script.name
}

resource "cloudflare_worker_script" "conneg_script" {
  name    = format("%s_conneg_script", replace(var.cname_record_value, ".", ""))
  content = file("scripts/index.js")

  plain_text_binding {
    name = "script_config_uri"
    text = var.script_config_uri
  }
}

resource "cloudflare_page_rule" "baressl" {
  depends_on = [cloudflare_page_rule.canonicalurl]
  zone_id    = var.cloudflare_zone_id
  target     = format("%s/*", var.a_record_name)
  priority   = 1
  actions {
    always_use_https = true
  }
}

resource "cloudflare_page_rule" "canonicalurl" {
  zone_id  = var.cloudflare_zone_id
  target   = format("www.%s/*", var.a_record_name)
  priority = 2
  actions {
    forwarding_url {
      url         = format("https://%s/$1", var.a_record_name)
      status_code = 301
    }
  }
}
